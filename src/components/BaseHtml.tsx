import Html from '@kitajs/html';

export const BaseHtml: Html.Component<{
	title?: string;
	head?: string;
}> = ({ title: safeTitle, head: safeHead, children }) => (
	<>
		{'<!doctype html>'}
		<html lang="en">
			<head>
				<meta charset="UTF-8" />
				<meta
					name="viewport"
					content="width=device-width, initial-scale=0.75"
				/>
				<title>{safeTitle ?? 'catflags'}</title>
				<link
					rel="shortcut icon"
					type="image/png"
					href="/favicon.png"
				></link>
				{safeHead}
			</head>
			<body>{children}</body>
		</html>
	</>
);
