import Html from '@kitajs/html';
import { BaseHtml } from '../components/BaseHtml';

const presets = {
	rainbow: 'e50000,ff8d00,ffee00,028121,004cff,770088',
	trans: '5bcffb,f5abb9,ffffff,f5abb9,5bcffb',
	nb: 'fcf431,fcfcfc,9d59d2,282828',
	bi: 'd60270,d60270,9b4f96,0038a8,0038a8',
	pan: 'ff1c8d,ffd700,1ab3ff',
	lesbian: 'd62800,ff9b56,ffffff,d462a6,a40062',
	ace: '000000,a4a4a4,ffffff,810081',
};

export const HomePage: Html.Component<{}> = ({}) => (
	<BaseHtml
		title="catflags"
		head={(<link rel="stylesheet" href="/home.css" />) as string}
	>
		<main>
			<h1>catflags</h1>

			<div class="links">
				{Object.entries(presets).map(([k, v]) => (
					<a href="#" onclick={`setStripes('${v}')`}>
						{k}
					</a>
				))}
			</div>

			<form>
				<div>
					<label for="stripes">stripes:</label>
					<input
						id="stripes"
						value="5bcefa,f5a9b8,ffffff,f5a9b8,5bcefa"
					/>
				</div>

				<div>
					<label for="density">density:</label>
					<input
						id="density"
						type="range"
						min="0"
						max="200"
						value="100"
					/>
				</div>

				<div>
					<label for="speed">speed:</label>
					<input
						id="speed"
						type="range"
						min="0"
						max="200"
						value="100"
					/>
				</div>

				<div>
					<label for="smooth">smooth:</label>
					<input id="smooth" type="checkbox" checked="true" />
				</div>
			</form>

			<iframe width="500" height="300" src="/flag" />

			<a id="full-size" href="/flag">
				(full size)
			</a>

			<div>
				<label for="embed">embed:</label>
				<input
					id="embed"
					maxlength="0"
					onclick="this.select()"
					readonly="true"
				/>
			</div>

			<div class="links">
				<a
					target="_blank"
					href="https://gitlab.com/hazel_meow/catflags"
				>
					code
				</a>
				<span>/</span>
				<a target="_blank" href="https://meows.zip">
					meows.zip
				</a>
			</div>
		</main>

		<script src="/home.js"></script>
	</BaseHtml>
);
