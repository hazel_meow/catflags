import Html from '@kitajs/html';
import type { Static } from '@sinclair/typebox';
import { t } from 'elysia';
import { BaseHtml } from '../components/BaseHtml';

export const flagQuery = t.Object({
	c: t.Optional(t.String()),
	d: t.Optional(t.String()),
	s: t.Optional(t.String()),
	m: t.Optional(t.String()),
});

export const FlagPage: Html.Component<{
	params: Static<typeof flagQuery>;
}> = ({ params }) => (
	<BaseHtml
		title="flag"
		head={(<link rel="stylesheet" href="/flag.css" />) as string}
	>
		<main>
			<canvas id="canvas"></canvas>
		</main>

		<script>
			window.PARAMS ={' '}
			{JSON.stringify({
				c: params.c ?? 'ffffff',
				d: parseFloat(params.d ?? '100') / 100,
				s: parseFloat(params.s ?? '100') / 100,
				m: (params.m ?? 'y') == 'y' ? true : false,
			})}
		</script>
		<script src="/meow.js"></script>
	</BaseHtml>
);
