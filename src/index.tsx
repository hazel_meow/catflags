import { html } from '@elysiajs/html';
import staticPlugin from '@elysiajs/static';
import Html from '@kitajs/html';
import { Elysia } from 'elysia';
import { FlagPage, flagQuery } from './pages/FlagPage';
import { HomePage } from './pages/HomePage';

const port = parseInt(process.env.PORT ?? '3000');

const app = new Elysia()
	.use(html())
	.use(
		staticPlugin({
			prefix: '/',
		})
	)
	.get('/', () => <HomePage />)
	.get('/flag', ({ query }) => <FlagPage params={query} />, {
		query: flagQuery,
	})
	.listen(port);

console.log(`running at ${app.server?.hostname}:${app.server?.port}`);
