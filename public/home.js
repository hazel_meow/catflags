const iframe = document.querySelector('iframe');
const fullSizeLink = document.getElementById('full-size');
const stripes = document.getElementById('stripes');
const density = document.getElementById('density');
const speed = document.getElementById('speed');
const smooth = document.getElementById('smooth');
const embed = document.getElementById('embed');

const onChange = () => {
	const url = `/flag?c=${stripes.value}&d=${density.value}&s=${
		speed.value
	}&m=${smooth.checked ? 'y' : 'n'}`;
	iframe.src = url;
	fullSizeLink.href = url;
	embed.value = `<iframe src="${window.location.origin}${url}"></iframe>`;
};

const form = document.querySelector('form');
form.addEventListener('change', onChange);
form.addEventListener('submit', (e) => {
	e.preventDefault();
	onChange();
	return false;
});

onChange();

function setStripes(s) {
	stripes.value = s;
	onChange();
}
