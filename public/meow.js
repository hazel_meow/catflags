// hiiiiiiii

const sourceImage = new Image();
sourceImage.src = '/oneko.png';

// https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
const hexToRgb = (hex) =>
	hex
		.replace(
			/^#?([a-f\d])([a-f\d])([a-f\d])$/i,
			(m, r, g, b) => '#' + r + r + g + g + b + b
		)
		.substring(1)
		.match(/.{2}/g)
		.map((x) => parseInt(x, 16));

const makeColoredImage = (r, g, b) => {
	const canvas = document.createElement('canvas');
	canvas.width = sourceImage.width;
	canvas.height = sourceImage.height;

	const ctx = canvas.getContext('2d');

	ctx.drawImage(sourceImage, 0, 0);

	const imageData = ctx.getImageData(
		0,
		0,
		sourceImage.width,
		sourceImage.height
	);
	const data = imageData.data;

	for (let i = 0; i < data.length; i += 4) {
		// black is 0,0,0
		if (data[i]) {
			data[i] = r;
			data[i + 1] = g;
			data[i + 2] = b;
		}
	}

	ctx.putImageData(imageData, 0, 0);

	const image = new Image();
	image.src = canvas.toDataURL();
	return image;
};
const coloredImageCache = {};
const getColoredImage = (r, g, b) => {
	const key = `${r}_${g}_${b}`;
	if (!coloredImageCache[key]) {
		coloredImageCache[key] = makeColoredImage(r, g, b);
	}
	return coloredImageCache[key];
};

class Renderer {
	constructor(baseImageData) {
		this.lastTimestamp = 0;
		this.cats = [];

		this.canvas = document.getElementById('canvas');
		this.context = this.canvas.getContext('2d');

		this.baseImageData = baseImageData;
		this.imageDataCache = {};

		this.resize();
		this.populate();
	}

	getColoredImageData(color) {
		return this.baseImageData;
	}

	resize() {
		this.width = window.innerWidth;
		this.height = window.innerHeight;

		this.canvas.width = this.width;
		this.canvas.height = this.height;
	}

	populate() {
		this.cats = [];

		const params = window.PARAMS;

		const stripes = params.c
			.split(',')
			.map((s) => s.trim().replace('#', '').replace(/^/, '#'));
		const numStripes = stripes.length;

		const densityMult = params.d;
		const globalSpeedMult = params.s;

		stripes.forEach((r, i) => {
			const yMin = (this.height / numStripes) * i;
			const yRange = this.height / numStripes;

			const xCount = (this.width / 32) * densityMult;
			const yCount = (yRange / 32) * densityMult;

			for (let i = 0; i < xCount * yCount; i++) {
				const yOffset = yMin + Math.random() * yRange;

				// -.5 to .5
				const speedRand = Math.random() - 0.5;
				const animTimeMax =
					(150 + speedRand * 75) * (1 / globalSpeedMult);
				const speed = (1 / animTimeMax) * 20;

				this.cats.push({
					x: Math.random() * this.width,
					y: yOffset,
					direction: Math.random() > 0.5 ? -1 : 1,
					speed,
					color: hexToRgb(r),
					animTime: Math.random() * animTimeMax,
					animTimeMax,
					anim: Math.random() > 0.5,
				});
			}
		});

		// y-sort !!
		this.cats = this.cats.sort((a, b) => a.y - b.y);
	}

	render(timestamp) {
		let dt = timestamp - this.lastTimestamp;
		this.lastTimestamp = timestamp;

		// when window isnt focused
		if (dt > 1000) {
			dt = 1000;
		}

		const ctx = this.context;

		ctx.fillStyle = 'white';
		ctx.fillRect(0, 0, this.width, this.height);

		this.cats.forEach((cat) => {
			cat.animTime += dt;
			while (cat.animTime > cat.animTimeMax) {
				cat.anim = !cat.anim;
				cat.animTime -= cat.animTimeMax;

				if (!window.PARAMS.m) {
					cat.x += cat.direction * cat.speed * cat.animTimeMax;
				}
			}

			if (window.PARAMS.m) {
				cat.x += cat.direction * cat.speed * dt;
			}

			if (cat.x < 0) {
				cat.direction = 1;
			} else if (cat.x > this.width) {
				cat.direction = -1;
			}

			const sx = (cat.anim ? 32 : 0) + (cat.direction == 1 ? 0 : 64);

			const image = getColoredImage(...cat.color);

			ctx.drawImage(
				image,
				sx,
				0,
				32,
				32,
				Math.round(cat.x) - 16,
				Math.round(cat.y) - 16,
				32,
				32
			);
		});

		requestAnimationFrame(this.render.bind(this));
	}
}

const setup = () => {
	if (!sourceImage.complete) {
		setTimeout(() => {
			setup();
		}, 10);
		return;
	}

	const renderer = new Renderer();

	window.addEventListener('resize', () => {
		renderer.resize();
		renderer.populate();
	});

	requestAnimationFrame(renderer.render.bind(renderer));
};

setup();
